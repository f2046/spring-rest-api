# Deploy
> 2021 12 20 Rafael Ferreira

Activate profile linux  
`$ export spring_profiles_active=<profile>`

Activate profile windows  
`$ set spring_profiles_active=<profile>`

Local run  
`$ ./mvnw spring-boot:run`

Local run with profile  
`$ ./mvnw spring-boot:run -Dspring-boot.run.profiles=development`

Run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## Build

Clean target  
`$ ./mvnw clean`

Build  
`$ ./mvnw package`

Docker build  
```sh
$ git clone https://gitlab.com/f2046/spring-rest-api.git
$ cd spring-rest-api
$ docker build -t spring-rest-api:latest .
```

## CI/CD

`$ sudo apk add gitlab-runner`

```
sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "szbpq7HzUeJzvzdAMJQc" \
--executor "shell" \
--description "docker-lab-node1" \
--tag-list "develop_server_tag" \
--run-untagged="false" \
--locked="false" \
--access-level="not_protected"
```

`$ gitlab-runner run &`
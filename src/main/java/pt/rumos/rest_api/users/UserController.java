package pt.rumos.rest_api.users;

import java.util.List;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/user")
    public String getAllUsers(Model m) {
        List<User> entities = service.findAll();
        return new Gson().toJson(entities);
    }
    
    
}

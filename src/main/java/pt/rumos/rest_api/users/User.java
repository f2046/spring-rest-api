package pt.rumos.rest_api.users;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="USER")
public class User {
    
    @Id
    private Long id;
    
    private String name;

    private String email;

    private String password;
}
